const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var jsdom = require('jsdom');
const { JSDOM } = jsdom;

const { document } = (new JSDOM('')).window;
global.document = global.document || document;

var fs = require('fs');
var $ = jQuery = require('jquery');
require('./jquery.csv');

var color_csv = './shades.csv';

const colorSchema  = new Schema({
    name:String,
    shadeCode:String,
    primaryColor:String,
    hexCode:String,
    DateofAddition:Date,
});
const Color = mongoose.model('color', colorSchema);

// fs.readFile(color_csv, 'UTF-8', function (err, csv) {
//     if (err) { console.log(err); }
//     $.csv.toArrays(csv, {}, function (err, data) {
//         if (err) { console.log(err); }
//         var categoryArr = [];
//         for (var i = 1, len = data.length; i < len; i++) {
//             new Color({
//                 name: data[i][4],
//                 shadeCode: data[i][1],
//                 primaryColor:data[i][3],
//                 hexCode:data[i][2],
//                 DateofAddition: new Date(),
//             }).save();
//             // if(!categoryArr.includes(data[i][3])){
//             //     categoryArr.push(data[i][3]);
//             // }
//             // console.log(i);
//             // console.log(categoryArr);
//             // console.log("total size is " + categoryArr.length);
//         }
//     });
// });
module.exports = Color;
