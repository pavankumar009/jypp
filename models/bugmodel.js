const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bugReportSchema  = new Schema({
    DateofRegistration:Date,
    dealercode:String,
    dealername:String,
    model:String,
    imeinumber:String,
    andversion:String,
    neroversion:String,
    lastsession:String,
    batterystatus:String,
    bugtype:String,
    description:String,
    city:String,

});

const BugReport = mongoose.model('bugreport', bugReportSchema);
module.exports = BugReport;