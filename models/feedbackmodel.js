const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const feedbackSchema  = new Schema({
    username:String,
    email:String,
    rating:String,
    comment:String,
});

const Feedback = mongoose.model('feedback', feedbackSchema);
module.exports = Feedback;
