const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const adminSchema  = new Schema({
    username:String,
    googleId:String,
    email:String
});



const Admin = mongoose.model('admin', adminSchema);

//create a default admin if no user exists
Admin.findOne({email:'harprasad@ensoimmersive.com'},function(err,currentUser){
    if(err){
        console.log(err);
        return;
    }
    if(currentUser){
        //do nothing if user exists
    }else{
        new Admin({
            username:"harprasad",
            email:"harprasad@ensoimmersive.com",
            googleId:"nana",
        }).save();
    }
});


module.exports = Admin;
