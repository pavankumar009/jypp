const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const changePasswordSchema  = new Schema({
    username:String,
    email:String
});

const ChangePassword = mongoose.model('changepassword', changePasswordSchema);
module.exports = ChangePassword;
