const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const customerDataSchema  = new Schema({
    DateofRegistration:Date,
    vendorname:String,
    vendorid:String,
    vendornumber:String,
    customername:String,
    gender:String,
    email:String,
    number:String,
    likedcolors:[String],
    selectedcolors:[String],
    onebhksilver:[String],
    onebhkgold:[String],
    twobhkgold:[String],
    exterior:[String],
    city:String,
    state:String,
    time:String,
    sessiontime:Number,
});

const CustomerEntry = mongoose.model('customerdata', customerDataSchema);

// for(var i = 0 ; i < 10 ; i++){
//     new CustomerEntry({
//         customername:"HP",
//         email:"harprasad@ensoimmersive.com",
//         number:"9040437295",
//         location:"-15.623028, 15.388667",
//         gender:"Male",
//         likedcolors:['#FFB0BB','#BBAABB','#FFAABB'],
//         vendorname:"vickey",
//         vendornumber:"8585446520",
//         vendorid:"GHFGII786567557454",
//         DateofRegistration:new Date(),
//     }).save();
// }
module.exports = CustomerEntry;