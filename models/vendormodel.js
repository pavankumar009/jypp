const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const vendorSchema  = new Schema({
    username:String,
    email:String,
    password:String,
    number:String,
    dealercode:String,
    pincode:String,
    location:String,
    state:String,
    DateofRegistration:Date,

    model:String,
    imeinumber:String,
    andversion:String,
    neroversion:String,
    lastsession:String,

});



const Vendor = mongoose.model('vendor', vendorSchema);
// for(var i = 0 ; i < 200 ; i++){
//     new Vendor({
//         username:"HP" + i,
//         email:"harprasad@ensoimmersive.com",
//         number:"9040437295",
//         password:"abc123456",
//         DateofRegistration:new Date(),
//     }).save();
// }
module.exports = Vendor;
