const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const customerSchema  = new Schema({
    customername:String,
    gender:String,
    email:String,
    number:String,
    DateofRegistration:Date,
});

const newCustomerEntry = mongoose.model('customer', customerSchema);
module.exports = newCustomerEntry;