var express = require('express');
var app = express();
var keys = require('./keysfile');
const cookieSession = require('cookie-session');
const passport = require('passport');
var hbs = require('express-handlebars');
var bodyParser = require('body-parser');



app.engine('hbs',hbs({extname:'hbs',defaultLayout:'layout',layoutsDir:__dirname + '/views'}));
app.set('view engine', 'hbs');
app.set('views','./views');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json({limit:8000})); 

module.exports = app;

var dbutil = require('./connection');
dbutil.connectToServer(function(err){
    if(err){
        console.log("Mongo db error " + err);
    }else{
        app.use(cookieSession({
            maxAge:24*60*60*1000,
            keys:[keys.session.cookieKey],
        }));
        app.use(passport.initialize());
        app.use(passport.session());        
        app.use(require('./controllers'));
        app.listen(3000);
    }
})

