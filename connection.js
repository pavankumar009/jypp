var db;
var dbName = 'NerolacDB';
var url = 'mongodb://localhost:27017/nodekb?readPreference=primary';
var mongoclient = require('mongodb').MongoClient;
const mongoose = require('mongoose');

module.exports = {
    connectToServer: function(callback ) {
        mongoose.connect(url,(err)=>{
            return callback(err);
        });
      }
}