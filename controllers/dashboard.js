var express = require('express')
  , router = express.Router()
var authchek = require('./authCheck');
var customer_data = require('../models/customerdata');
var vendor_data = require('../models/vendormodel');
var customer = require('../models/customer');
var bug_report = require('../models/bugmodel');

router.get('/',authchek,function(req, res) {
  res.render('layout',{layout:'dashboard'});
});

router.get('/getentries',authchek,function(req, res) {
  var from = parseInt(req.query.from);
  customer_data.find().sort({DateofRegistration:-1}).skip(from).limit(100).then((results) => {
    if(results){
        customer_data.find().count().then((count) =>{
          var response = { entries: results , count : count };
          res.send(response);
        });
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/search',function (req, res) {
  var _search = req.query.search;
  var _name = req.query.name;
  var from = parseInt(req.query.from);
  // var query = {};
  // query[_name] = value;
  customer_data.find({[_name]: _search}).skip(from).limit(100).then((results) => {
    if (results) {
      customer_data.find().count().then((count) => {
        var response = { entries: results, count: count };
        res.send(response);
      });
    } else {
      
      console.log("No entries found");
      res.send("");
    }
  });
});

router.get('/logsweekdata4',function(req, res) {
  var name = req.query.name;
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } },
        { vendorid : name }
    ]
     }
    },
    {"$group" : 
      {_id:{"$dayOfMonth":"$DateofRegistration"}, count:{$sum:1}}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/avgweekdata4',function(req, res) {
  var name = req.query.name;
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  customer_data.aggregate([
    {
      $match: { 
        $and : [
          { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } },
          { vendorid : name }
      ]
       }
      },
    {"$group" : 
      {_id:{"$dayOfMonth":"$DateofRegistration"},count:{"$avg": "$sessiontime"}}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/maxweekdata4',function(req, res) {
  var name = req.query.name;
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  customer_data.aggregate([
    {
      $match: { 
        $and : [
          { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } },
          { vendorid : name }
      ]
       }
      },
    {"$group" : 
      {_id:{"$dayOfMonth":"$DateofRegistration"},count:{"$max": "$sessiontime"}}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});


router.get('/minweekdata4',function(req, res) {
  var name = req.query.name;
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  customer_data.aggregate([
    {
      $match: { 
        $and : [
          { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } },
          { vendorid : name }
      ]
       }
      },
    {"$group" : 
      {_id:{"$dayOfMonth":"$DateofRegistration"},count:{"$min": "$sessiontime"}}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/logsmonthdata4',function(req, res) {
  var name = req.query.name;
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer_data.aggregate([
    {
      $match: { 
        $and : [
          { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } },
          { vendorid : name }
      ]
       }
      },
    {"$group" : 
      {_id:{"$dayOfMonth":"$DateofRegistration"},count:{"$sum": 1}}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/avgmonthdata4',function(req, res) {
  var name = req.query.name;
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer_data.aggregate([
    {
      $match: { 
        $and : [
          { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } },
          { vendorid : name }
      ]
       }
      },
    {"$group" : 
      {_id:{"$dayOfMonth":"$DateofRegistration"},count:{"$avg": "$sessiontime"}}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/maxmonthdata4',function(req, res) {
  var name = req.query.name;
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer_data.aggregate([
    {
      $match: { 
        $and : [
          { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } },
          { vendorid : name }
      ]
       }
      },
    {"$group" : 
      {_id:{"$dayOfMonth":"$DateofRegistration"},count:{"$max": "$sessiontime"}}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});


router.get('/minmonthdata4',function(req, res) {
  var name = req.query.name;
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer_data.aggregate([
    {
      $match: { 
        $and : [
          { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } },
          { vendorid : name }
      ]
       }
      },
    {"$group" : 
      {_id:{"$dayOfMonth":"$DateofRegistration"},count:{"$min": "$sessiontime"}}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/logsyeardata4',function(req, res) {
  var name = req.query.name;
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  console.log(yearstart);
  customer_data.aggregate([
    {
      $match: { 
        $and : [
          { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } },
          { vendorid : name }
      ]
       }
      },
    {"$group" : 
      {_id:{"$month":"$DateofRegistration"},count:{"$sum": 1}}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/avgyeardata4',function(req, res) {
  var name = req.query.name;
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  customer_data.aggregate([
    {
      $match: { 
        $and : [
          { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } },
          { vendorid : name }
      ]
       }
      },
    {"$group" : 
      {_id:{"$month":"$DateofRegistration"},count:{"$avg": "$sessiontime"}}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/maxyeardata4',function(req, res) {
  var name = req.query.name;
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  customer_data.aggregate([
    {
      $match: { 
        $and : [
          { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } },
          { vendorid : name }
      ]
       }
      },
    {"$group" : 
      {_id:{"$month":"$DateofRegistration"},count:{"$max": "$sessiontime"}}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});


router.get('/minyeardata4',function(req, res) {
  var name = req.query.name;
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  customer_data.aggregate([
    {
      $match: { 
        $and : [
          { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } },
          { vendorid : name }
      ]
       }
      },
    {"$group" : 
      {_id:{"$month":"$DateofRegistration"},count:{"$min": "$sessiontime"}}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/drlookup',function(req, res) {
  var _dealercode = req.query.name;
  console.log(_dealercode);
  vendor_data.findOne({dealercode:_dealercode}).then((result) => {
    if(result){
          var response = {entries: result};
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/top3weekdata2',function(req, res) {
  var name = " "+req.query.name;
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  console.log(weekstart);
  console.log(weekend);
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } },
        { city : name }
    ]
     }
    },
  { "$group": {
      "_id": {
      "day":{$dayOfMonth:"$DateofRegistration"},
      "vendorname": "$vendorname"
      },
      "vcount":{"$sum":1}
  }},
  {$sort:{"vcount":-1}},
  { "$group": {
      "_id": "$_id.day",
      "topdealers": { 
          "$push": { 
              "vendorname": "$_id.vendorname",
              "count": "$vcount"
          },
      },
      "count": { "$sum": "$vcount" }
  }},
  {$sort:{"_id":1}} 
]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/avgweekdata2',function(req, res) {
  var name = " "+req.query.name;
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } },
        { city : name }
    ]
     }
    },
    {"$group" : 
      {_id:{
        $dayOfMonth:"$DateofRegistration"
          },
       count:{"$avg": "$sessiontime" }}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/maxweekdata2',function(req, res) {
  var name = " "+req.query.name;
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } },
        { city : name }
    ]
     }
    },
    {"$group" : 
      {_id:{
        $dayOfMonth:"$DateofRegistration"
          },
       count:{"$max": "$sessiontime" }}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});


router.get('/minweekdata2',function(req, res) {
  var name = " "+req.query.name;
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } },
        { city : name }
    ]
     }
    },
    {"$group" : 
      {_id:{
        $dayOfMonth:"$DateofRegistration"
          },
       count:{"$min": "$sessiontime" }}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/top3monthdata2',function(req, res) {
  var name = " "+req.query.name;
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } },
        { city : name }
    ]
     }
    },
  { "$group": {
      "_id": {
      "day":{$dayOfMonth:"$DateofRegistration"},
      "vendorname": "$vendorname"
      },
      "vcount":{"$sum":1}
  }},
  {$sort:{"vcount":-1}},
  { "$group": {
      "_id": "$_id.day",
      "topdealers": { 
          "$push": { 
              "vendorname": "$_id.vendorname",
              "count": "$vcount"
          },
      },
      "count": { "$sum": "$vcount" }
  }},
  {$sort:{"_id":1}} 
]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/avgmonthdata2',function(req, res) {
  var name = " "+req.query.name;
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } },
        { city : name }
    ]
     }
    },
    {"$group" : 
      {_id:{
        $dayOfMonth:"$DateofRegistration"
          },
       count:{"$avg": "$sessiontime" }}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/maxmonthdata2',function(req, res) {
  var name = " "+req.query.name;
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } },
        { city : name }
    ]
     }
    },
    {"$group" : 
      {_id:{
        $dayOfMonth:"$DateofRegistration"
          },
       count:{"$max": "$sessiontime" }}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});


router.get('/minmonthdata2',function(req, res) {
  var name = " "+req.query.name;
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } },
        { city : name }
    ]
     }
    },
    {"$group" : 
      {_id:{
        $dayOfMonth:"$DateofRegistration"
          },
       count:{"$min": "$sessiontime" }}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/top3yeardata2',function(req, res) {
  var name = " "+req.query.name;
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } },
        { city : name }
    ]
     }
    },
  { "$group": {
      "_id": {
      "day":{$month:"$DateofRegistration"},
      "vendorname": "$vendorname"
      },
      "vcount":{"$sum":1}
  }},
  {$sort:{"vcount":-1}},
  { "$group": {
      "_id": "$_id.day",
      "topdealers": { 
          "$push": { 
              "vendorname": "$_id.vendorname",
              "count": "$vcount"
          },
      },
      "count": { "$sum": "$vcount" }
  }},
  {$sort:{"_id":1}} 
]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/avgyeardata2',function(req, res) {
  var name = " "+req.query.name;
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } },
        { city : name }
    ]
     }
    },
    {"$group" : 
      {_id:{
        $month:"$DateofRegistration"
          },
       count:{"$avg": "$sessiontime" }}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/maxyeardata2',function(req, res) {
  var name = " "+req.query.name;
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } },
        { city : name }
    ]
     }
    },
    {"$group" : 
      {_id:{
        $month:"$DateofRegistration"
          },
       count:{"$max": "$sessiontime" }}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});


router.get('/minyeardata2',function(req, res) {
  var name = " "+req.query.name;
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  customer_data.aggregate([
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } },
        { city : name }
    ]
     }
    },
    {"$group" : 
      {_id:{
        $month:"$DateofRegistration"
          },
       count:{"$min": "$sessiontime" }}
    }, 
    {$sort:{"_id":1}}
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/drlookup1',function(req, res) {
  var name = " "+req.query.name;
  customer_data.findOne({city : name}).then((result) => {
    if(result){
          var response = {entries: result};
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/totallogsweekdata2',function(req, res) {
  var city_state = " "+req.query.cs;
  var _gender = req.query.gender;
  var _type = req.query.type;
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  customer_data.aggregate([  
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } },
        { $or : [ { city  : city_state}, { state : city_state}] },
        { gender : _gender }
    ]
     }
    },
    {$unwind: _type},
    {$group : 
      {_id:{ 
        day:{$dayOfMonth:"$DateofRegistration"},
        shortlistedcolors: _type
        },
       vcount:{"$sum": 1 }}
    }, 
    {$sort:{"_id.day":1,"vcount":-1}},
        { "$group": {
        "_id": "$_id.day",
        "topcolors": { 
            "$push": { 
                "shortlistedcolors": "$_id.shortlistedcolors",
                "count": "$vcount"
            },
        },
        "count": { "$sum": "$vcount" }
    }},
    {$sort:{"_id":1}} 
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/avgsessiontimeweekdata2',function(req, res) {
  var city_state = " "+req.query.cs;
  var _gender = req.query.gender;
  var _type = req.query.type;
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  customer_data.aggregate([  
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } },
        { $or : [ { city  : city_state}, { state : city_state}] },
        { gender : _gender }
    ]
     }
    },
    {$unwind: "$selectedcolors"},
    {$group : 
      {_id:{ 
        day:{$dayOfMonth:"$DateofRegistration"},
        selectedcolors: "$selectedcolors"
        },
       vcount:{"$sum": 1 }}
    }, 
    {$sort:{"_id.day":1,"vcount":-1}},
        { "$group": {
        "_id": "$_id.day",
        "topcolors": { 
            "$push": { 
                "selectedcolors": "$_id.selectedcolors",
                "count": "$vcount"
            },
        },
        "count": { "$sum": "$vcount" }
    }},
    {$sort:{"_id":1}} 
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/totallogsmonthdata2',function(req, res) {
  var city_state = " "+req.query.cs;
  var _gender = req.query.gender;
  var _type = req.query.type;
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer_data.aggregate([  
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } },
        { $or : [ { city  : city_state}, { state : city_state}] },
        { gender : _gender }
    ]
     }
    },
    {$unwind: _type},
    {$group : 
      {_id:{ 
        day:{$dayOfMonth:"$DateofRegistration"},
        shortlistedcolors: _type
        },
       vcount:{"$sum": 1 }}
    }, 
    {$sort:{"_id.day":1,"vcount":-1}},
        { "$group": {
        "_id": "$_id.day",
        "topcolors": { 
            "$push": { 
                "shortlistedcolors": "$_id.shortlistedcolors",
                "count": "$vcount"
            },
        },
        "count": { "$sum": "$vcount" }
    }},
    {$sort:{"_id":1}} 
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/avgsessiontimemonthdata2',function(req, res) {
  var city_state = " "+req.query.cs;
  var _gender = req.query.gender;
  var _type = req.query.type;
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer_data.aggregate([  
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } },
        { $or : [ { city  : city_state}, { state : city_state}] },
        { gender : _gender }
    ]
     }
    },
    {$unwind: "$selectedcolors"},
    {$group : 
      {_id:{ 
        day:{$dayOfMonth:"$DateofRegistration"},
        selectedcolors: "$selectedcolors"
        },
       vcount:{"$sum": 1 }}
    }, 
    {$sort:{"_id.day":1,"vcount":-1}},
        { "$group": {
        "_id": "$_id.day",
        "topcolors": { 
            "$push": { 
                "selectedcolors": "$_id.selectedcolors",
                "count": "$vcount"
            },
        },
        "count": { "$sum": "$vcount" }
    }},
    {$sort:{"_id":1}} 
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/totallogsyeardata2',function(req, res) {
  var city_state = " "+req.query.cs;
  var _gender = req.query.gender;
  var _type = req.query.type;
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  customer_data.aggregate([  
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } },
        { $or : [ { city  : city_state}, { state : city_state}] },
        { gender : _gender }
    ]
     }
    },
    {$unwind: _type},
    {$group : 
      {_id:{ 
        day:{$month:"$DateofRegistration"},
        shortlistedcolors: _type
        },
       vcount:{"$sum": 1 }}
    }, 
    {$sort:{"_id.day":1,"vcount":-1}},
        { "$group": {
        "_id": "$_id.day",
        "topcolors": { 
            "$push": { 
                "shortlistedcolors": "$_id.shortlistedcolors",
                "count": "$vcount"
            },
        },
        "count": { "$sum": "$vcount" }
    }},
    {$sort:{"_id":1}} 
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/avgsessiontimeyeardata2',function(req, res) {
  var city_state = " "+req.query.cs;
  var _gender = req.query.gender;
  var _type = req.query.type;
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  customer_data.aggregate([  
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } },
        { $or : [ { city  : city_state}, { state : city_state}] },
        { gender : _gender }
    ]
     }
    },
    {$unwind: "$selectedcolors"},
    {$group : 
      {_id:{ 
        day:{$month:"$DateofRegistration"},
        selectedcolors: "$selectedcolors"
        },
       vcount:{"$sum": 1 }}
    }, 
    {$sort:{"_id.day":1,"vcount":-1}},
        { "$group": {
        "_id": "$_id.day",
        "topcolors": { 
            "$push": { 
                "selectedcolors": "$_id.selectedcolors",
                "count": "$vcount"
            },
        },
        "count": { "$sum": "$vcount" }
    }},
    {$sort:{"_id":1}} 
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/drlookup2',function(req, res) {
  var city_state = " "+req.query.cs;
  customer_data.findOne({ $or: [ {city:city_state}, {state:city_state} ] }).then((result) => {
    if(result){
          var response = {entries: result};
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/totallogsweekdata3',function(req, res) {
    var color = req.query.color;
    var type = req.query.type;
    var type1 = '$'+type;
    var gender = req.query.gender;
    var weekstart = req.query.weekstart;
    var weekend = req.query.weekend
    customer_data.aggregate([
      {$unwind:type1},
      {
        $match: { 
          $and : [
            { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } },
            { [type] : color},
            { gender : gender}
        ]
         }
        },
      {$group : 
        {_id:{ 
          day:{$dayOfMonth:"$DateofRegistration"},
          enteredcolor: type1
          },
         vcount:{"$sum": 1 }}
      }, 
      {$sort:{"_id.day":1,"vcount":-1}},
          { "$group": {
          "_id": "$_id.day",
          "shortlists": { 
              "$push": { 
                  "shortlistedcolor": "$_id.enteredcolor",
                  "count": "$vcount"
              },
          }
      }},
      {$sort:{"_id":1}} 
    ]).then((results) => {
      if(results){
            var response = { results: results };
            res.send(response);
      }else{
          console.log("No entries found");
          res.send("");
      }
    });
});

router.get('/avgsessiontimeweekdata3',function(req, res) {
  var color = req.query.color;
  var gender = req.query.gender;
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  customer_data.aggregate([
    {$unwind:"$selectedcolors"},
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } },
        { selectedcolors : color},
        { gender : gender}
    ]
     }
    },
    {$group : 
      {_id:{ 
        day:{$dayOfMonth:"$DateofRegistration"},
        selectedcolor:"$selectedcolors",
        },
       vcount:{"$sum": 1 }}
    }, 
    {$sort:{"_id.day":1,"vcount":-1}},
        { "$group": {
        "_id": "$_id.day",
        "selects": { 
            "$push": { 
                "selectedcolor": "$_id.selectedcolor",
                "count": "$vcount"
            },
        }
    }},
    {$sort:{"_id":1}} 
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/totallogsmonthdata3',function(req, res) {
  var color = req.query.color;
  var type = req.query.type;
  var type1 = '$'+type;
  var gender = req.query.gender;
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer_data.aggregate([
    {$unwind: type1},
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } },
        { [type] : color},
        { gender : gender}
    ]
     }
    },
    {$group : 
      {_id:{ 
        day:{$dayOfMonth:"$DateofRegistration"},
        enteredcolor: type1,
        },
       vcount:{"$sum": 1 }}
    }, 
    {$sort:{"_id.day":1,"vcount":-1}},
        { "$group": {
        "_id": "$_id.day",
        "shortlists": { 
            "$push": { 
                "shortlistedcolor": "$_id.enteredcolor",
                "count": "$vcount"
            },
        }
    }},
    {$sort:{"_id":1}} 
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/avgsessiontimemonthdata3',function(req, res) {
  var color = req.query.color;
  var gender = req.query.gender;
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer_data.aggregate([
    {$unwind:"$selectedcolors"},
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } },
        { selectedcolors : color},
        { gender : gender}
    ]
     }
    },
    {$group : 
      {_id:{ 
        day:{$dayOfMonth:"$DateofRegistration"},
        selectedcolor:"$selectedcolors",
        },
       vcount:{"$sum": 1 }}
    }, 
    {$sort:{"_id.day":1,"vcount":-1}},
        { "$group": {
        "_id": "$_id.day",
        "selects": { 
            "$push": { 
                "selectedcolor": "$_id.selectedcolor",
                "count": "$vcount"
            },
        }
    }},
    {$sort:{"_id":1}} 
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/totallogsyeardata3',function(req, res) {
  var color = req.query.color;
  var type = req.query.type;
  var type1 = '$'+type;
  var gender = req.query.gender;
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  console.log(color);
  console.log(type);
  console.log(type1);
  console.log(gender);
  console.log(yearstart);
  customer_data.aggregate([
    {$unwind: type1},
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } },
        { [type] : color},
        { gender : gender}
    ]
     }
    },
    {$group : 
      {_id:{ 
        day:{$month:"$DateofRegistration"},
        enteredcolor: type1,
        },
       vcount:{"$sum": 1 }}
    }, 
    {$sort:{"_id.day":1,"vcount":-1}},
        { "$group": {
        "_id": "$_id.day",
        "shortlists": { 
            "$push": { 
                "shortlistedcolor": "$_id.enteredcolor",
                "count": "$vcount"
            },
        }
    }},
    {$sort:{"_id":1}} 
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/avgsessiontimeyeardata3',function(req, res) {
  var color = req.query.color;
  var gender = req.query.gender;
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  customer_data.aggregate([
    {$unwind:"$selectedcolors"},
    {
    $match: { 
      $and : [
        { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } },
        { selectedcolors : color},
        { gender : gender}
    ]
     }
    },
    {$group : 
      {_id:{ 
        day:{$month:"$DateofRegistration"},
        selectedcolor:"$selectedcolors",
        },
       vcount:{"$sum": 1 }}
    }, 
    {$sort:{"_id.day":1,"vcount":-1}},
        { "$group": {
        "_id": "$_id.day",
        "selects": { 
            "$push": { 
                "selectedcolor": "$_id.selectedcolor",
                "count": "$vcount"
            },
        }
    }},
    {$sort:{"_id":1}} 
  ]).then((results) => {
    if(results){
          var response = { results: results };
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/drlookup3',function(req, res) {
  var color = req.query.color;
  var type = req.query.type;
  var type1 = '$'+type;
  var gender = req.query.gender;
  customer_data.aggregate([
    {$unwind:type1},
    { 
      $match : { [type] : color } 
    }
  ]).then((result) => {
    if(result){
          var response = {entries: result};
          res.send(response);
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});


router.get('/customerweekdata',function(req,res){
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  customer.aggregate([
    { $match : { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } } } ,
        {$group : 
        {_id:{$dayOfMonth:"$DateofRegistration"},
         count:{"$sum": 1 }
         }
      }
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"vendor data not found"});
    }
  });
});

router.get('/vendorweekdata',function(req,res){
  var weekstart = req.query.weekstart;
  var weekend = req.query.weekend;
  vendor_data.aggregate([
    { $match : { "DateofRegistration" : { $gte: new Date(weekstart), $lte: new Date(weekend) } } } ,
        {$group : 
        {_id:{$dayOfMonth:"$DateofRegistration"},
         count:{"$sum": 1 }
         }
      }
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"vendor data not found"});
    }
  });
});

router.get('/customermonthdata',function(req,res){
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  customer.aggregate([
    { $match : { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } } } ,
        {$group : 
        {_id:{$dayOfMonth:"$DateofRegistration"},
         count:{"$sum": 1 }
         }
      }
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"vendor data not found"});
    }
  });
});

router.get('/vendormonthdata',function(req,res){
  var monthstart = req.query.monthstart;
  var monthend = req.query.monthend;
  vendor_data.aggregate([
    { $match : { "DateofRegistration" : { $gte: new Date(monthstart), $lte: new Date(monthend) } } } ,
        {$group : 
        {_id:{$dayOfMonth:"$DateofRegistration"},
         count:{"$sum": 1 }
         }
      }
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"vendor data not found"});
    }
  });
});

router.get('/customeryeardata',function(req,res){
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  customer.aggregate([
    { $match : { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } } } ,
        {$group : 
        {_id:{$month:"$DateofRegistration"},
         count:{"$sum": 1 }
         }
      }
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"vendor data not found"});
    }
  });
});

router.get('/vendoryeardata',function(req,res){
  var yearstart = req.query.yearstart;
  var yearend = req.query.yearend;
  vendor_data.aggregate([
    { $match : { "DateofRegistration" : { $gte: new Date(yearstart), $lte: new Date(yearend) } } } ,
        {$group : 
        {_id:{$month:"$DateofRegistration"},
         count:{"$sum": 1 }
         }
      }
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"vendor data not found"});
    }
  });
});

router.get('/vendordaydata',function(req,res){
  vendor_data.aggregate([
    {"$group" : 
      {_id:{"$dayOfMonth":"$DateofRegistration"}, count:{$sum:1}}
    }, 
    {$sort:{"_id.day":1}}
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"vendor data not found"});
    }
  });
});


router.get('/customerdaydata',function(req,res){
  customer.aggregate([
    {"$group" : 
      {_id:{"$dayOfMonth":"$DateofRegistration"}, count:{$sum:1}}
    }, 
    {$sort:{"_id.day":1}}
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"customer data not found"});
    }
  });
});

router.get('/vendorweekdata',function(req,res){
  vendor_data.aggregate([
    {"$group" : 
      {_id:{"$week":"$DateofRegistration"}, count:{$sum:1}}
    }, 
    {$sort:{"_id.day":1}}
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"vendor data not found"});
    }
  });
});


router.get('/customerweekdata',function(req,res){
  customer.aggregate([
    {"$group" : 
      {_id:{"$week":"$DateofRegistration"}, count:{$sum:1}}
    }, 
    {$sort:{"_id.day":1}}
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"customer data not found"});
    }
  });
});

router.get('/vendormonthdata',function(req,res){
  vendor_data.aggregate([
    {"$group" : 
      {_id:{"$month":"$DateofRegistration"}, count:{$sum:1}}
    }, 
    {$sort:{"_id.day":1}}
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"vendor data not found"});
    }
  });
});


router.get('/customermonthdata',function(req,res){
  customer.aggregate([
    {"$group" : 
      {_id:{"$month":"$DateofRegistration"}, count:{$sum:1}}
    }, 
    {$sort:{"_id.day":1}}
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"customer data not found"});
    }
  });
});

router.get('/vendoryeardata',function(req,res){
  vendor_data.aggregate([
    {"$group" : 
      {_id:{"$year":"$DateofRegistration"}, count:{$sum:1}}
    }, 
    {$sort:{"_id.day":1}}
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"vendor data not found"});
    }
  });
});


router.get('/customeryeardata',function(req,res){
  customer.aggregate([
    {"$group" : 
      {_id:{"$year":"$DateofRegistration"}, count:{$sum:1}}
    }, 
    {$sort:{"_id.day":1}}
  ]).sort({_id:1}).then((result) => {
    if(result)
    {
      var response = { data: result };
      res.send(response)
        return;
    }else{
      res.send({msg:"customer data not found"});
    }
  });
});



router.get('/maxsessiontime',function(req, res) {
  customer_data.aggregate([{ $sort : {sessiontime : -1} }]).then((results) => {
    if(results){
          var response = { session: results };
          res.send(response)
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/minsessiontime',function(req, res) {
  customer_data.aggregate([{ $sort : {sessiontime : 1} }]).then((results) => {
    if(results){
          var response = { session: results };
          res.send(response)
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});


router.get('/hottestgeolocation',function(req, res) {
  customer_data.aggregate([{$group : { _id : '$city', count : {$sum : 1}}}]).sort({count:-1}).limit(1).then((results) => {
    if(results){
        var response = { session: results };
        res.send(response)
    }else{
        console.log("No entries found");
        res.send("");
    }
  });
});


router.get('/avgsessiontime',function(req, res) {

  customer_data.distinct("sessiontime").then((results) => {
    if(results)   {   
      vendor_data.find().count().then((count) => {
      var response = { session: results, count: count };
      res.send(response);
    });
  }
      else{
        console.log("No entries found");
        res.send("");
    }
  });
});


router.get('/bestcolor',function(req, res) {
  customer_data.distinct("likedcolors").then((results) => {
    if(results)   {  
      var resu = results.sort(); 
      var response = { bestcolor: resu};
      res.send(response);
  }
      else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/getuniquecust',function(req, res) {

  customer_data.distinct("email").then((results) => {
    if(results)   {   
      vendor_data.find().count().then((count) => {
      var response = { customer: results.length, vendor: count };
      res.send(response);
    });
  }
      else{
        console.log("No entries found");
        res.send("");
    }
  });
});

router.get('/bestdealer',function(req, res) {

  customer_data.aggregate([  
    { $group: {    
        _id:      { vendorid: "$vendorid" },    
        uniqueIds:{ $addToSet: "$vendorname" },     
        count:    { $sum: 1 }}},  
    { $match: {count: { $gte: 2 }}},  
    { $sort : {count : -1}},  
    { $limit : 10 } ]).then((results) => {
    if(results)   {   
      var response = { bestdealer: results};
      res.send(response);
  }
      else{
        console.log("No entries found");
        res.send("");
    }
  });
});

module.exports = router;