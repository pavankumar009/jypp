var express = require('express')
  , router = express.Router();

// router.use('/dashboard', require('./dashboard'));
// router.use('/bugreports', require('./bugreports'));
router.use('/main', require('./main'));
// router.use('/analytics', require('./analytics'));
// router.use('/logout',require('./logout'));
// router.use('/manage_colors', require('./manage_colors'));
// router.use('/manage_vendors', require('./manage_vendors'));
// router.use('/api',require('./api'));
// router.get('/about', function(req, res) {
//   res.send('Learn about us');
// });

router.get('/', function(req, res) {
    res.redirect('/main');
});
module.exports = router