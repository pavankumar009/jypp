var nodemailer = require('nodemailer');
var mailhbs = require('nodemailer-express-handlebars');
var keys = require('../keysfile');




var mailoptions = {
    viewEngine: {
        extname: '.hbs',
        layoutsDir: 'views/email/',
        defaultLayout : 'template',
        partialsDir : 'views/partials/',
        helpers:{
            inc: function(value){return parseInt(value) + 1;}
        },
    },
    viewPath: 'views/email/',
    extName: '.hbs'
};

const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    auth: {
        user: keys.emailer.user,
        pass: keys.emailer.pass,
    }
});

transporter.use('compile',mailhbs(mailoptions));

module.exports = {
    sendEmail: function(data) {
        var maillist = [
            data.cmail,
            data.deemail,
        ];
        transporter.sendMail({
            from: 'Support <support@ensoimmersive.com>',
            to: maillist,
            subject: 'Nerolac - Colour My Space Experience',
            template: 'template',
            context: {
                 CNAME : data.cname,
                 CEMAIL: data.cmail,
                 CNUM: data.cnum,
                 SELECTED: data.selected,
                 VENAME: data.vname,
                 VEMAIL: data.vemail,
                 VENUM: data.vnum,
                 HASONESILVER: data.hasOneBhkSilver,
                 HASONEGOLD: data.hasOneBhkGold,
                 HASTWOGOLD: data.hasTwobhkGold,
                 ONESILVERARR: data.oneBhkSilver,
                 ONEGOLDARR: data.oneBhkGold,
                 TWOGOLDARR: data.twoBhkGold,
                 EXTERIORARR: data.exterior,
                 HASEXTERIOR: data.hasExterior,
            }
        },  function (error, response) {
            if(error){
                console.log(error);
            }
            else{
                console.log("email send success");
            }
            transporter.close();
        });
    }
}