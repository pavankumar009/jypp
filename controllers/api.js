var express = require('express')
  , router = express.Router()
var nodemailer = require('nodemailer');
var mailhbs = require('nodemailer-express-handlebars');
var keys = require('../keysfile');
var customer_data = require('../models/customerdata');
var vendor_data = require('../models/vendormodel');
var color_data = require('../models/colormodel');
var feedback_data = require('../models/feedbackmodel');
var customer = require('../models/customer');
var bug_report = require('../models/bugmodel');
var emailer = require('../controllers/emailer');
var client = require('twilio')(keys.twilio.accountSid, keys.twilio.authToken); 
router.post('/makeentry',function(req, res) {

    var vendoremail = req.body.ve_email;
    var deemail = req.body.vendor_email;
    var password = req.body.ve_password;
    var _customername = req.body.cu_name;
    var _customeremail = req.body.cu_email;
    var _customernumber = req.body.cu_number;
    var _city = req.body.cu_city;
    var _state = req.body.cu_state;
    var _customerlikedcolors = req.body.cu_likedcolors;
    var _customerselectedcolors = req.body.cu_historycolors;
    var _culikednames = req.body.cu_likednames;
    var _cuselected = req.body.cu_selected;
    var _cugender = req.body.cu_gender;
    var _cutime = req.body.time;
    var _cusession = req.body.cu_sessiontime;
    var _groupcolors = JSON.parse(req.body.cu_groupcolors);
    var onebhksilver = req.body.One_BHK_Silver;
    var onebhkgold = req.body.One_BHK_Gold;
    var twobhkgold = req.body.Two_BHK_Gold;
    var exterior = req.body.Exterior;
    var _vendorname ;
    var _vendornumber;
    var _vendorCode;
    var _hasOnebhkSilver;
    var _hasOnebhkGold;  
    var _hasTwobhkGold;  
    var _hasExterior;    
    var _oneBHKsilver = [];//_groupcolors.One_BHK_Silver;
    var _oneBHKGold =  [];//_groupcolors.One_BHK_Gold;
    var _twoBHKGold = [];//_groupcolors.Two_BHK_Gold;
    var _exterior = [];
    _groupcolors.One_BHK_Silver.forEach(element => {
      _oneBHKsilver.push({
        color:element,
        name:_culikednames[_customerlikedcolors.indexOf(element)],
      })
    });

    _groupcolors.One_BHK_Gold.forEach(element => {
      _oneBHKGold.push({
        color:element,
        name:_culikednames[_customerlikedcolors.indexOf(element)],
      })
    });

    _groupcolors.Two_BHK_Gold.forEach(element => {
      _twoBHKGold.push({
        color:element,
        name:_culikednames[_customerlikedcolors.indexOf(element)],
      })
    });

    _groupcolors.Exterior.forEach(element => {
      _exterior.push({
        color:element,
        name:_culikednames[_customerlikedcolors.indexOf(element)],
      })
    });

   _hasOnebhkSilver = _groupcolors.One_BHK_Silver.length > 0 ? true : false;
   _hasOnebhkGold   = _groupcolors.One_BHK_Gold.length > 0 ? true : false;
   _hasTwobhkGold   = _groupcolors.Two_BHK_Gold.length > 0 ? true : false;
   _hasExterior    = _groupcolors.Exterior.length > 0 ? true : false;
    //authcheck vendor
    vendor_data.findOne({dealercode:vendoremail}).then((result) => {
      if(result)
      {
        //check password 
        if(result.password !== password)
        {
          res.send({msg:"Auth failed wrong password"});
          return;
        }else{
          _vendorCode = result.dealercode;
          _vendorname = result.username;
          _vendornumber = result.number;

          customer.findOne({email:_customeremail}).then((result) => {
            if(result){
              res.send({msg:"Customer already exists"});
            }
            else {
              new customer({
                customername:_customername,
                gender:_cugender,
                email:_customeremail,
                number:_customernumber,
                DateofRegistration:new Date(),
              }).save();
              res.send({msg:"new customer added"});
            }
          });
          //make an aentry
          new customer_data({
            customername:_customername,
            email:_customeremail,
            number:_customernumber,
            gender:_cugender,
            city:_city,
            state:_state,
            likedcolors:_customerlikedcolors,
            selectedcolors:_customerselectedcolors,
            onebhksilver:onebhksilver,
            onebhkgold:onebhkgold,
            twobhkgold:twobhkgold,
            exterior:exterior,
            vendorname:_vendorname,
            vendornumber:_vendornumber,
            vendorid:_vendorCode,
            DateofRegistration:new Date(),
            sessiontime:_cusession,
            time:_cutime,
          }).save();
          emailer.sendEmail({
            vname:_vendorname,
            vnum:_vendornumber,
            vemail:vendoremail,
	    deemail:deemail,
            cname:_customername,
            cnum:_customernumber,
            cmail:_customeremail,
            liked:_customerlikedcolors,
            likednames:_culikednames,
            selected:_cuselected,
            hasOneBhkSilver:_hasOnebhkSilver,
            hasOneBhkGold:_hasOnebhkGold,
            hasTwobhkGold:_hasTwobhkGold,
            hasExterior:_hasExterior,
            oneBhkSilver:_oneBHKsilver,
            oneBhkGold:_oneBHKGold,
            twoBhkGold:_twoBHKGold,
            exterior : _exterior,
          });
          res.send({msg:"success"});
        }
      }
      else{
        res.send({msg:"vendor not found"});
      }
    });
});

router.get('/getcolors', function (req, res) {
    var from = parseInt(req.query.from);
    color_data.find().sort({"shadeCode":1}).skip(from).limit(100).then((results) => {
      if (results) {
        color_data.find().count().then((count) => {
          var response = { entries: results, count: count };
          res.send(response);
        });
      } else {
        res.send("No colours");
      }
    });
});

router.get('/lastcolorupdate', function(req,res){
  color_data.findOne().sort({"DateofAddition":-1}).then((result) => {
    if(result){
      var date = result.DateofAddition;
      res.send({
        lastupdate:date,
        day : date.getDate(),       // yields date
        month:date.getMonth() + 1,    // yields month (add one as '.getMonth()' is zero indexed)
        year: date.getFullYear(),  // yields year
        hour:date.getHours(),     // yields hours 
        minute:date.getMinutes(), // yields minutes
        second:date.getSeconds(),
      });
    }
    else{
      res.send({msg:"No colors in DB"});
    }
  });

});

router.get('/getcolors/:primarycolor', function (req,res) {
  var primarycolor = req.params.primarycolor;
  if(primarycolor){
     color_data.find({primaryColor:primarycolor}).sort({"shadeCode":1}).then((results)=>{
       if(results.length > 0)
        res.send(results);
      else{
        res.send({msg:"No colurs found for " + primarycolor});
      }
    });
  }
});

router.post('/velogin',function(req,res){
  var vendorid = req.body.ve_email;
  var password = req.body.ve_password;

  var model = req.body.model;
  var imeinumber = req.body.imeinumber;
  var andversion = req.body.andversion;
  var neroversion = req.body.neroversion;
  var lastsession = req.body.lastsession;

  vendor_data.findOne({dealercode:vendorid}).then((result) => {
    if(result)
    {
      //check password 
      if(result.password !== password)
      {
        res.send({msg:"Auth failed wrong password"});
        return;
      }else
      {
        vendor_data.findOneAndUpdate({dealercode:vendorid},{$set:{model:model,imeinumber:imeinumber,andversion:andversion,neroversion:neroversion,lastsession:lastsession}}).then((results) => {
          if(results)
          {
              res.send({msg:"Details updated successfully"});
              return;
          }else{
            res.send({msg:"vendor not found"});
          }
        });

        var ve_data = {
                        name: result.username,
                        number: result.number,
                        dealercode: result.dealercode,
                        email: result.email,
                        city:result.location,
                        state:result.state,
                      } 
        res.send({msg:"success",data: ve_data});
      }
    }else{
      res.send({msg:"vendor not found"});
    }
  });
});

router.post('/mycustomers',function(req,res){
  var vendorid = req.body.ve_email;
  var password = req.body.ve_password;
  var _customeremail = req.body.cu_email;
  var _vendorId;

  vendor_data.findOne({dealercode:vendorid}).then((result) => {
    if(result)
    {
      if(result.password !== password)
      {
        res.send({msg:"Auth failed wrong password"});
        return;
      }else{
        _vendorId = result.id;
        //fetch the existing customers
        customer_data.find({email:_customeremail,vendorid:_vendorId}).then((entries) => {
          if(entries.length > 0){
            res.send({data:entries,msg:"success"});
          }
          else{
            res.send({data:"",msg:"customer not found"});
          }
        });
      }
    }
    else{
      res.send({msg:"vendor not found"});
    }
  });

});

router.post('/addvendor',function (req,res) {
  var qemail = req.body.email;
  
  var admin = req.body.admin;
  var password = req.body.password;

  if(admin !==  keys.admin.name || password !== keys.admin.password){
    res.send({message:"Auth Failed"});
    return;
  }
  vendor_data.findOne({ email: qemail }).then((results) => {
    if (results) {
      res.send({ message: "user already exists" });
    } else {
      new vendor_data({
        username: req.body.name,
        email: req.body.email,
        number: req.body.number,
        password: keys.admin.password,
        dealercode:req.body.dealercode,
        pincode:req.body.pincode,
        location:req.body.location,
        state:req.body.state,
        DateofRegistration: new Date(),
      }).save();
      res.send({ message: req.body.name + " user created" });
    }
  });
});

router.post('/feedback',function(req,res){
  new feedback_data({
    email:req.body.email,
    rating:req.body.rating,
    comment:req.body.comment,
  }).save();
  res.send({resp:"Done"});
});

router.post('/bugreport',function(req,res){
  new bug_report({
    DateofRegistration:new Date(),
    dealercode:req.body.ve_email,
    dealername:req.body.ve_name,
         model:req.body.model,
    imeinumber:req.body.imeinumber,
    andversion:req.body.andversion,
   neroversion:req.body.neroversion,
   lastsession:req.body.lastsession,
 batterystatus:req.body.batterystatus,
       bugtype:req.body.bugtype,
   description:req.body.description,
          city:req.body.city,
  }).save();
  res.send({msg:"success"});
});


router.post('/sendemail',function(req,res){
  var vendor_id = req.body.ve_email;

  vendor_data.findOne({dealercode:vendor_id}).then((result) => {
    if(result)
    {
var myemail = result.email;
var mailoptions = {
  viewEngine: {
      extname: '.hbs',
      layoutsDir: 'views/',
      defaultLayout : 'resetpass',
      partialsDir : 'views/partials/',
      helpers:{
          inc: function(value){return parseInt(value) + 1;}
      },
  },
  viewPath: 'views/',
  extName: '.hbs'
};

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  auth: {
      user: keys.emailer.user,
      pass: keys.emailer.pass,
  }
});

transporter.use('compile',mailhbs(mailoptions));

  // send mail with defined transport object
  transporter.sendMail({
    from: 'Support <support@ensoimmersive.com>',
    to: myemail,
    subject: 'Nerolac - Colour My Space Experience',
    template: 'resetpass'
},  function (error, response) {
    if(error){
      res.send({msg:"Email for reset password not sent"});
    }
    else{
      res.send({msg:"Email for reset password sent"});
    }
    transporter.close();
});
    }else{
      res.send({msg:"vendor not found"});
    }
  });
});

router.post('/randompass',function(req,res){
  var vendor_id = req.body.ve_email;
  var chars = "ABCDEFGHiJKLMNOP1234567890";
  var pass = "";
  
  vendor_data.findOne({dealercode:vendor_id}).then((result) => {
    if(result)
    {
        for (var x = 0; x < 6; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
vendor_data.findOneAndUpdate({dealercode:vendor_id},{$set:{password:pass}}).then((result) => {
  if(result)
  {
      res.send({msg:"Password updated successfully"});
      return;
  }else{
    res.send({msg:"vendor not found"});
  }
});

var resnum1 = result.number;
var phonenumber;
var resnum = resnum1.substring(0, 3);
if(resnum !== '+91'  && resnum1.length == 10){
  phonenumber = '+91'.concat(resnum1);
}
else if(resnum === '+91'  && resnum1.length == 13){
  phonenumber = result.number;
}

client.messages.create({ 
  body: 'Nerolac - Colour My Space: '+pass+' is your new password.', 
  from: '+13605487039',       
  to: phonenumber, 
  }).then((message) => console.log(message.sid)).done();
var myemail = result.email;
var mailoptions = {
  viewEngine: {
      extname: '.hbs',
      layoutsDir: 'views/',
      defaultLayout : 'randompass',
      partialsDir : 'views/partials/',
      helpers:{
          inc: function(value){return parseInt(value) + 1;}
      },
  },
  viewPath: 'views/',
  extName: '.hbs'
};
const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  auth: {
      user: keys.emailer.user,
      pass: keys.emailer.pass,
  }
});
transporter.use('compile',mailhbs(mailoptions));
  transporter.sendMail({
    from: 'Support <support@ensoimmersive.com>',
    to: myemail,
    subject: 'Nerolac - Colour My Space Experience',
    template: 'randompass',
    context:{
        newpass : pass
    }
},  function (error, response) {
    if(error){
      res.send({msg:"Email for random password not sent"});
    }
    else{
      res.send({msg:"Email for random password sent"});
    }
    transporter.close();
});
    }else{
      res.send({msg:"vendor not found"});
    }
  });
});

router.post('/resetpass',function(req,res){
  var vendor_id = req.body.ve_email;
  var newpass = req.body.ve_newpassword;
  vendor_data.findOneAndUpdate({dealercode:vendor_id},{$set:{password: newpass}}).then((result) => {
    if(result)
    {
        res.send({msg:"Password updated successfully"});
        return;
    }else{
      res.send({msg:"vendor not found"});
    }
  });
});

router.post('/changepass',function(req,res){
  var vendor_id = req.body.ve_email;
  var existingpass = req.body.ve_existingpass;
  var newpass = req.body.ve_newpass;
  vendor_data.findOneAndUpdate({dealercode:vendor_id},{$set:{password: newpass}}).then((result) => {
    if(result)
    {
        res.send({msg:"Password updated successfully"});
        return;
    }else{
      res.send({msg:"vendor not found"});
    }
  });
});


router.post('/existingcust',function(req,res){
  var vendor_number = req.body.ve_number;
  var cust_email = req.body.cu_email;
  customer_data.findOne({email:cust_email}).then((result) => {
    if(result)
    {
      //check email 
      if(result.vendornumber == vendor_number && result.email == cust_email)
      {
        var ve_data = {
          custname: result.customername,
          number: result.number,
          sessiontime:result.sessiontime,
          gender:result.gender,
        }
        res.send({msg:"success",data: ve_data}); 
        return;
      }else{
        res.send({msg:"customer not found"});
      }
    }else{
      res.send({msg:"vendor not found"});
    }
  });
});


module.exports = router;