var express = require('express')
  , router = express.Router()
var authchek = require('./authCheck');
var vendor_data = require('../models/vendormodel');

router.get('/',authchek,function (req, res) {
  res.render('layout', { layout: 'manage_vendors' });
});

router.get('/getentries',authchek,function (req, res) {
  var from = parseInt(req.query.from);
  vendor_data.find().skip(from).limit(100).then((results) => {
    if (results) {
      vendor_data.find().count().then((count) => {
        var response = { entries: results, count: count };
        res.send(response);
      });
    } else {
      console.log("Npentries found");
      res.send("");
    }
  });
});

router.get('/search',function (req, res) {
  var _number = req.query.number;
  var _name = req.query.name;
  console.log(req.query);
  vendor_data.find({ $or: [ { username:_name }, { number: _number } ] }).limit(100).then((results) => {
    if (results) {
        vendor_data.find().count().then((count) => {
        var response = { entries: results, count: count };
        res.send(response);
      });
    } else {
      console.log("Npentries found");
      res.send("");
    }
  });
});

router.post('/addvendor',function (req, res) {
  var qemail = req.body.email;
  vendor_data.findOne({ email: qemail }).then((results) => {
    if (results) {
      res.send({ message: "user already exists" });
    } else {
      new vendor_data({
        username: req.body.name,
        email: req.body.email,
        number: req.body.number,
        password: req.body.password,
        dealercode:req.body.dealercode,
        pincode:req.body.pincode,
        location:req.body.location,
        state:req.body.state,
        DateofRegistration: new Date(),
      }).save();
      res.send({ message: req.body.name + " user created" });
    }
  });
});

module.exports = router;