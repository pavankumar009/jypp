var express = require('express')
  , router = express.Router()
var authchek = require('./authCheck');
var customer_data = require('../models/customerdata');
var vendor_data = require('../models/vendormodel');
var customer = require('../models/customer');
var bug_report = require('../models/bugmodel');

router.get('/',authchek,function(req, res) {
  res.render('layout',{layout:'bugreports'});
});

router.get('/getentries',authchek,function (req, res) {
    var from = parseInt(req.query.from);
    bug_report.find().skip(from).limit(100).then((results) => {
      if (results) {
        bug_report.find().count().then((count) => {
          var response = { entries: results, count: count };
          res.send(response);
        });
      } else {
        console.log("Npentries found");
        res.send("");
      }
    });
  });
  
  router.get('/search',function (req, res) {
    var _number = req.query.number;
    var _name = req.query.name;
    console.log(req.query);
    bug_report.find({ $or: [ { dealercode:_name }, { bugtype: _number } ] }).limit(100).then((results) => {
      if (results) {
          bug_report.find().count().then((count) => {
          var response = { entries: results, count: count };
          res.send(response);
        });
      } else {
        console.log("Npentries found");
        res.send("");
      }
    });
  });
  
  module.exports = router;