var auth = function(req, res, next) {
    if (req.user)
      return next();
    else
    {
      console.log("Auth failed");
      res.redirect('/login');
      
    }
};

module.exports = auth;