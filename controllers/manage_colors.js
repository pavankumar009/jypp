var express = require('express')
  , router = express.Router()
var authchek = require('./authCheck');
var color_data = require('../models/colormodel');

router.get('/',authchek,function(req, res) {
  res.render('layout',{layout:'manage_colors'});
});

router.get('/getcolors',authchek, function (req, res) {
  var from = parseInt(req.query.from);
  color_data.find().sort({"shadeCode":1}).skip(from).limit(100).then((results) => {
    if (results) {
      color_data.find().count().then((count) => {
        var response = { entries: results, count: count };
        res.send(response);
      });
    } else {
      console.log("Npentries found");
      res.send("");
    }
  });
});

router.post('/addcolor',function (req, res) {
  var hexcode = req.body.colorHex;
  color_data.findOne({ hexCode:hexcode }).then((results) => {
    if (results) {
      res.send({ message: "color code already exists" });
    } else {
      new color_data({
        name:req.body.name,
        shadeCode:req.body.colorShade,
        primaryColor:req.body.colorPrimaryName,
        hexCode:req.body.colorHex,
        DateofAddition: new Date(),
      }).save();
      res.send({ message: req.body.name + " created" });
    }
  });
});

router.get('/search',function (req, res) {
  var _code = req.query.code;
  var _name = req.query.name;
  color_data.find({ $or: [ { hexCode:_code }, { name: _name } ] }).limit(100).then((results) => {
    if (results) {
      color_data.find().count().then((count) => {
        var response = { entries: results, count: count };
        res.send(response);
      });
    } else {
      
      console.log("No entries found");
      res.send("");
    }
  });
});

module.exports = router;