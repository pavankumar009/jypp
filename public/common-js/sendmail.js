$(function () {    
    $("#notify-email-form").submit(function (e) {
      e.preventDefault();
      var form_data = $(this).serialize(); 
      $.ajax({
        type: "POST", 
        url: "contact.php",
        dataType: "json", // Add datatype
        data: form_data
      }).done(function (data) {
          console.log(data);
          alert("Mail Sent! We will get in touch soon.");
      }).fail(function (data) {
          console.log(data);
          alert("Mail Not Sent! Try after some time.");
      });
    }); 
  });