var pagerInitialised = false;
var TableEntry = function (data) {
    var self = this;
    this.dor = ko.observable(data.DateofRegistration);
    this.vendorname = ko.observable(data.vendorname);
    this.vendorid = ko.observable(data.vendorid);
    this.vendornumber = ko.observable(data.vendornumber);
    this.name = ko.observable(data.customername);
    this.email = ko.observable(data.email);
    this.phone = ko.observable(data.number);
    this.gender = ko.observable(data.gender);
    this.likedcolor = ko.observableArray();
    this.city = ko.observable(data.city);
    
    data.likedcolors.forEach(element => {
        self.likedcolor.push(new ColorObj({
            code:element,
        }));
    });
console.log(ko.observable(data.vendorname));
};

var ColorObj = function (color)
{
    this.code = ko.observable(color.code);
}

function getTable(data) {
    var url = "/dashboard/getentries"
    var data = data;
    $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });
}

function callback(data,status){
    if (status == "success") {
console.log(data);
if(data.entries.length == 0){
    $('#datanotfound').show();
}
        vm.customerentries([]);
        for (var i = 0; i < data.entries.length; i++) {
            $('#datanotfound').hide();

            var entry = data.entries[i];
            vm.customerentries.push(new TableEntry(entry));
        }
        vm.totalitems(data.count);
        if(!pagerInitialised){
            pagerInitialised = true;
            resetPager();
        }
    }
}
$(document).ready(function () {
    $('#datanotfound').hide();
});

var ViewModel = function () {
    var self = this;
    this.customerentries = ko.observableArray();
    this.totalitems = ko.observable(0);
    this.skipto = ko.observable(0);
    this.enterSearchType = ko.observable("");
    this.enterToSearch = ko.observable("");
    this.pagenumbeClick = function(page,e){
        var skipto = (page-1) * 100;
        self.skipto(skipto);
        getTable({from:skipto});        
    };
    this.searchData = function()
    {
        var url = "/dashboard/search"
        var data = { 
            name:self.enterSearchType(),
            search:self.enterToSearch(),
            from:0,
        };
        if(data.search == '')
        {
            getTable({from:0});
            resetPager();
        }
        else
        {
        $.getJSON(url, data, callback ).fail(function () { console.log("Error occured")});   
        } 
    };
}
var vm = new ViewModel();
ko.applyBindings(vm);
getTable({from:0});
resetPager();

function resetPager()
{
    $("#pager").pagination({
        items: vm.totalitems(),
        itemsOnPage: 100,
        cssStyle: 'light-theme',
        onPageClick:vm.pagenumbeClick,
    });
}
