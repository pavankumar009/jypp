var pagerInitialised = false;
var VendorEntry = function (data) {
    var self = this;
    this.name = ko.observable(data.username);
    this.email = ko.observable(data.email);
    this.dor = ko.observable(data.DateofRegistration);
    this.number = ko.observable(data.number);
    this.vendorid = ko.observable(data.dealercode);
};


function getTable(data) {
    var url = "/manage_vendors/getentries"
    var data = data;
    $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });
}

function callback(data,status){
    console.log(data);
    if (status == "success") {
        vm.vendorlist([]);
        for (var i = 0; i < data.entries.length; i++) {
            var entry = data.entries[i];
            vm.vendorlist.push(new VendorEntry(entry));
        }
        vm.totalitems(data.count);
        if(!pagerInitialised){
            pagerInitialised = true;
            resetPager();
        }
    }
}

var ViewModel = function () {
    var self = this;

    this.dashboardVisibility = ko.observable(true);
    this.addVisibility = ko.observable(false);
    this.message = ko.observable("");

    this.vendorlist = ko.observableArray();
    this.totalitems = ko.observable(0);
    this.skipto = ko.observable(0);
    //------------form values-------------------//
    this.vendorName = ko.observable("");
    this.vendorEmail = ko.observable("");
    this.vendorNumber = ko.observable("");
    this.vendorPassword = ko.observable("");
    this.location = ko.observable("");
    this.dealerCode = ko.observable("");
    this.pinCode = ko.observable("");
    this.dealerState = ko.observable("");
    this.searchName = ko.observable("");
    this.searchNumber = ko.observable("");

    this.messageVisible = ko.computed(function(){
        if(self.message() == "")return false;
        else return true;
    })

    //-------------Onclick Functions--------------//
    this.pagenumbeClick = function(page,e){
        var skipto = (page-1) * 100;
        self.skipto(skipto);
        getTable({from:skipto});        
    }

    this.AddVendorClick = function()
    {
        self.dashboardVisibility(false);
        self.addVisibility(true);
    }
    this.cancelClick = function()
    {
        self.addVisibility(false);
        self.dashboardVisibility(true);
    }

    this.addVendor = function()
    {
        var url = "/manage_vendors/addvendor"
        var data = { 
            name:self.vendorName(),
            email:self.vendorEmail(),
            number:self.vendorNumber(),
            password:self.vendorPassword(),
            dealercode:self.dealerCode(),
            pincode:self.pinCode(),
            location:' '+self.location(),
            state:' '+self.dealerState(),
        };
        $.post(url, data, addvendorCallback ).fail(function () { console.log("Error occured")});    
    }

    this.searchVendor = function()
    {
        var url = "/manage_vendors/search"
        var data = { 
            name:self.searchName(),
            number:self.searchNumber(),
        };
        if(data.name == '' && data.number == '')
        {
            var url = "/manage_vendors/getentries"
            var data = data;
            $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });
        
    }
    else if(data.name == '')
    {
        var url = "/manage_vendors/search"
        var data = { 
            number:self.searchNumber(),
        };
        $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });
    }
    else if(data.number == '')
    {
        var url = "/manage_vendors/search"
        var data = { 
            name:self.searchName(),
        };
        $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });
    }
    else
     {
        $.getJSON(url, data, callback ).fail(function () { console.log("Error occured")});   } 
    }
}

function addvendorCallback(data,status)
{
    if(status == 'success'){
        vm.message(data.message);
        setTimeout(() => {
            vm.message("");
        }, 3000);
    }
}

var vm = new ViewModel();
ko.applyBindings(vm);
getTable({from:0});
resetPager();

function resetPager()
{
    $("#pager").pagination({
        items: vm.totalitems(),
        itemsOnPage: 100,
        cssStyle: 'light-theme',
        onPageClick:vm.pagenumbeClick,
    });
}
