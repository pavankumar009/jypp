var pagerInitialised = false;
var ColorEntry = function (data) {
    var self = this;
    this.name = ko.observable(data.name);
    this.primaryName = ko.observable(data.primaryColor);
    this.colorShade = ko.observable(data.shadeCode);
    this.colorHex = ko.observable(data.hexCode);
    this.dor = ko.observable(data.DateofAddition);
    this.colorId = ko.observable(data._id);
};


function getTable(data) {
    var url = "/manage_colors/getcolors"
    var data = data;
    $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });
}

function callback(data,status){
    if (status == "success") {
        console.log(data);
        vm.colorlist([]);
        for (var i = 0; i < data.entries.length; i++) {
            var entry = data.entries[i];
            vm.colorlist.push(new ColorEntry(entry));
        }
        vm.totalitems(data.count);
        if(!pagerInitialised){
            pagerInitialised = true;
            resetPager();
        }
    }
}

var ViewModel = function () {
    var self = this;

    this.dashboardVisibility = ko.observable(true);
    this.addVisibility = ko.observable(false);
    this.message = ko.observable("");

    this.colorlist = ko.observableArray();
    this.totalitems = ko.observable(0);
    this.skipto = ko.observable(0); 
    //------------form values-------------------//
    this.colorName = ko.observable("");
    this.colorPrimaryName = ko.observable("");
    this.colorShade = ko.observable("");
    this.colorHex = ko.observable("");
    this.searchCode = ko.observable("");
    this.searchName = ko.observable("");
    this.messageVisible = ko.computed(function(){
        if(self.message() == "")return false;
        else return true;
    })

    //-------------Onclick Functions--------------//
    this.pagenumbeClick = function(page,e){
        var skipto = (page-1) * 100;
        self.skipto(skipto);
        getTable({from:skipto});        
    }

    this.AddColorClick = function()
    {
        self.dashboardVisibility(false);
        self.addVisibility(true);
    }
    this.cancelClick = function()
    {
        self.addVisibility(false);
        self.dashboardVisibility(true);
    }

    this.addColor = function()
    {
        var url = "/manage_colors/addcolor"
        var data = { 
            name:self.colorName(),
            colorPrimaryName:self.colorPrimaryName(),
            colorHex:self.colorHex(),
            colorShade:self.colorShade(),
        };
        $.post(url, data, addColorCallback ).fail(function () { console.log("Error occured")});    
    }

    this.searchColor = function()
    {
        var url = "/manage_colors/search"
        var data = { 
            name:self.searchName(),
            code:self.searchCode(),
        };
        if(data.name == '' && data.code == '')
        {
                var url = "/manage_colors/getcolors"
                var data = data;
                $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });
            
        }
        else if(data.name == '')
        {
            var url = "/manage_colors/search"
            var data = { 
                code:self.searchCode(),
            };
            $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });
        }
        else if(data.code == '')
        {
            var url = "/manage_colors/search"
            var data = { 
                name:self.searchName(),
            };
            $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });
        }
        else
        {
        $.getJSON(url, data, callback ).fail(function () { console.log("Error occured")});   
        } 
    }
}

function addColorCallback(data,status)
{
    if(status == 'success'){
        vm.message(data.message);
        setTimeout(() => {
            vm.message("");
        }, 3000);
    }
}

function resetPager()
{
    $("#pager").pagination({
        items: vm.totalitems(),
        itemsOnPage: 100,
        cssStyle: 'light-theme',
        onPageClick:vm.pagenumbeClick,
    });
}


var vm = new ViewModel();
ko.applyBindings(vm);
getTable({from:0});
resetPager();



