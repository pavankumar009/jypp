var pagerInitialised = false;
var VendorEntry = function (data) {
    var self = this;
    this.name = ko.observable(data.dealername);
    this.code = ko.observable(data.dealercode);
    this.nerover = ko.observable(data.neroversion);
    this.bugt = ko.observable(data.bugtype);
    this.andver = ko.observable(data.andversion);
    this.desc = ko.observable(data.description);
    this.model = ko.observable(data.model);
    this.imei = ko.observable(data.imeinumber);
    this.bat = ko.observable(data.batterystatus);
    this.last = ko.observable(data.lastsession);
    this.city = ko.observable(data.city);
    this.date = ko.observable(data.DateofRegistration);
};

function getTable(data) {
    var url = "/bugreports/getentries"
    var data = data;
    $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });
}

function callback(data,status){
    console.log(data);
    if (status == "success") {
        vm.vendorlist([]);
        for (var i = 0; i < data.entries.length; i++) {
            var entry = data.entries[i];
            vm.vendorlist.push(new VendorEntry(entry));
        }
        vm.totalitems(data.count);
        if(!pagerInitialised){
            pagerInitialised = true;
            resetPager();
        }
    }
}

$('[role="tablist"]').on("click", '[role="tab"]', function () {
    $('.panel-collapse.collapse').collapse('hide');
    $(this).next().collapse('show');
    $('.panel-heading').removeClass('active');
    $(this).addClass('active');
});

var ViewModel = function () {
    var self = this;
    this.message = ko.observable("");
    this.vendorlist = ko.observableArray();
    this.totalitems = ko.observable(0);
    this.skipto = ko.observable(0);
    //------------form values-------------------//
    this.vendorName = ko.observable("");
    this.vendorEmail = ko.observable("");
    this.vendorNumber = ko.observable("");
    this.vendorPassword = ko.observable("");
    this.location = ko.observable("");
    this.dealerCode = ko.observable("");
    this.pinCode = ko.observable("");
    this.dealerState = ko.observable("");
    this.searchName = ko.observable("");
    this.searchNumber = ko.observable("");
    this.messageVisible = ko.computed(function(){
        if(self.message() == "")return false;
        else return true;
    })
    //-------------Onclick Functions--------------//
    this.pagenumbeClick = function(page,e){
        var skipto = (page-1) * 100;
        self.skipto(skipto);
        getTable({from:skipto});        
    }

    this.searchVendor = function()
    {
        var url = "/bugreports/search"
        var data = { 
            name:self.searchName(),
            number:self.searchNumber(),
        };
        if(data.name == '' && data.number == '')
        {
            var url = "/bugreports/getentries"
            var data = data;
            $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });   
    }
    else if(data.name == '')
    {
        var url = "/bugreports/search"
        var data = { 
            number:self.searchNumber(),
        };
        $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });
    }
    else if(data.number == '')
    {
        var url = "/bugreports/search"
        var data = { 
            name:self.searchName(),
        };
        $.getJSON(url, data, callback).fail(function () { console.log("Error occured") });
    }
    else
     {
        $.getJSON(url, data, callback ).fail(function () { console.log("Error occured")});   } 
    }
}

var vm = new ViewModel();
ko.applyBindings(vm);
getTable({from:0});
resetPager();
function resetPager()
{
    $("#pager").pagination({
        items: vm.totalitems(),
        itemsOnPage: 100,
        cssStyle: 'light-theme',
        onPageClick:vm.pagenumbeClick,
    });
}
