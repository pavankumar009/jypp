
window.onresize = function()
{
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        //document.querySelector('a-scene').reload();    
    }
}

var roomButton = function (data) {
    var self = this;
    this.index = data.index;
    this.name = data.name;
    if(isMobile){
        this.image = ko.observable("../ui/WebVr/Thumbnail1.png");
    }else{
        this.image = ko.observable("ui/WebVr/Thumbnail1.png");
    }
    this.textcolor = ko.observable('white');
    this.onRoomClicked = function () {
        // self.image("/ui/WebVr/Thumbnail1h.png");
        // self.textcolor('#126acf');
        vm.roomClick(self.index);

    };
};

var ViewModel = function () {

    var self = this;
    this.notVR = ko.observable(true);
    this.isMinimized = ko.observable(false);
    this.currentroom = ko.observable(1);
    this.buttonList = ko.observableArray();
    this.animstate = ko.observable(true);
    this.animpaused = ko.observable(false);
    this.popupview = ko.observable(false);
    //------------------click functions---------------------//
    this.fullscreenClick = function()
    {
        var el = document.documentElement,
        rfs = el.requestFullscreen
        || el.webkitRequestFullScreen
        || el.mozRequestFullScreen
        || el.msRequestFullscreen;
        rfs.call(el);
    };
    this.threeSixtyClick = function ( ) {
        if(!self.animstate()){
            document.querySelector('#maincam').emit('start');
            self.animstate(true);
        }
        else{
            if(self.animpaused()){
                document.querySelector('#maincam').emit('start');
                self.animpaused(false);
            }else{
                self.animpaused(true);
                document.querySelector('#maincam').emit('pause');
            }

        }
    };

    this.arrowClick = function ( ) {
        var nowin = self.currentroom();
        nowin++;
        if(nowin > 11 ){
            nowin = 1;
        }
        self.currentroom(nowin);
        self.roomClick(nowin);
    };
    this.infoClick  = function ( ) {
        self.popupview(!self.popupview());
    }

    this.uitoggleClock = function( ) {
        $("#roomthumbs").slideToggle("slow");
        self.isMinimized(!self.isMinimized());
    }
    this.buttonstatus = ko.pureComputed(function(){
        if(self.isMinimized())
            return "minimized";
        else{
            return "maximized";
        }

    });

    this.goVR = function ( ) {
        document.querySelector('a-scene').enterVR();
    }
    this.exitVR = function () 
    {
        document.querySelector('a-scene').exitVR();
    }

    //------------------------- Utility functions--------------------//
    this.roomClick = function (index){
        self.resetButton();
        self.highlightButton(index);
        var entityId = '#Nav' + index;
        var entity = document.querySelector(entityId);
        entity.querySelector('.link').emit('click');
        self.currentroom(index);


        var arrows = document.querySelectorAll('.arrow');
        arrows.forEach(element => {
            element.setAttribute("visible",false);
        });
        //activate room specific arrows after some delay
        setTimeout(() => {
            var roomClass = ".room"+ index+ "nav";
            arrows = document.querySelectorAll(roomClass);
            arrows.forEach(element => {
                element.setAttribute("visible",true);
            });
        }, 1000);
    }

    this.resetButton = function()
    {
        self.buttonList().forEach(element => {
            if(isMobile)
                element.image("../ui/WebVr/Thumbnail1.png");
            else{
                element.image("ui/WebVr/Thumbnail1.png");
            }
            element.textcolor("white");
        });
    }
    this.highlightButton = function(index)
    {
        var button = self.buttonList()[index-1];
        if(isMobile)
            button.image("../ui/WebVr/Thumbnail1h.png");
        else{
            button.image("ui/WebVr/Thumbnail1h.png");
        }
        button.textcolor("#126acf");
    }

};


var vm = new ViewModel();
ko.applyBindings(vm);


for(i = 1 ; i <= 11 ; i++){
    var data = null;
    switch (i) {
        case 1:
            data = {index:i,name:"Boys Bedroom"};
            break;
        case 2:
            data = {index:i,name:"Girls Bedroom"};
            break;
        case 3:
            data = {index:i,name:"Living Room"};
            break;
        case 4:
            data = {index:i,name:"Dining Room"};
            break;
        case 5:
            data = {index:i,name:"Kitchen"};
            break;
        case 6:
            data = {index:i,name:"Confession Room"};
            break;
        case 7:
            data = {index:i,name:"Store Room"};
            break;
        case 8:
            data = {index:i,name:"Swimming Pool"};
            break;
        case 9:
            data = {index:i,name:"Garden"};
            break;
        case 10:
            data = {index:i,name:"Activity Area"};
            break;
        case 11:
            data = {index:i,name:"Bathroom"};
            break;
        default:
            console.log("error index of home");
            break;
    }
    //data = {index:i,name:"Boys Bedroom"};
    vm.buttonList.push(new roomButton(data));
};

function arrowClick(val,data) {

    if( data!= undefined && data.getAttribute("visible") === false){
        return;
    }
    console.log("You clicked on "+ val);
    vm.roomClick(val);
}
