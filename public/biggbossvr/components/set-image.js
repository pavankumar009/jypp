
/* global AFRAME */

/**
 * Component that listens to an event, fades out an entity, swaps the texture, and fades it
 * back in.
 */
AFRAME.registerComponent('set-image', {
  schema: {
    event: {type:'string'},
    on: {type: 'string'},
    target: {type: 'selector'},
    src: {type: 'string'},
    dur: {type: 'number', default: 300}
  },

  init: function () {
    var data = this.data;
    var el = this.el;

    this.setupFadeAnimation();
    el.addEventListener(data.on, function () {
      // Fade out image.
      data.target.emit('set-image-fade');
      // Wait for fade to complete.
      setTimeout(function () {
        // Set image.
        data.target.setAttribute('material', 'src', data.src);

        //Toggle navlinks
        let navlinks = document.getElementsByClassName("navlinks");

        //Set which navlinks are available in which room. in [position, rotation]
       let mapper = [1, //Entrance
                     1, //Kitchen
                     1, //Hall
                     1, //DiningArea
                     1, //Passage
                     1, //Bedroom1
                     1, //Toilet1
                     1, //Bedroom2
                     1, //Toilet2
                     1, //Bedroom3
                     1];//CommonToilet
        switch(data.src){
          case "#Entrance":
            mapper = [0,["-0.5 -1.5 5","0 -190 0"], ["-3 -1 -0.5","0 82 0"],0,0,0,0,0,0,0,0];
            break;
          case "#Kitchen":
            mapper = [["-1.5 -1 -4", "0 20 0"],0,0,0,0,0,0,0,0,0,0];
            break;
          case "#Hall":
            mapper = [["9 -1 0.5", "0 -90 0"],0,0,["4 -1 3.5","0 -130 0"],["3.5 -1 7", "0 -170 0"],0,0,0,0,0,0];
            break;
          case "#DiningArea":
            mapper = [["8 -1 -3", "0 -90 0"],0,["-3 -1 -0.5","0 82 0"],0,["-0.5 -2 7","0 -185 0"],0,0,0,0,0,0];
            break;
          case "#Passage":
            mapper = [0,0,["-0.8 -0.5 -6", "0 60 0"],["0.5 -0.7 -8", "0 -30  0"],0,["-4 -1 0", "0 -270 0"],0,["-3 -3 6", "0 -210 0"],0,["0.2 -2.5 5", "0 -180 0"],["2 -1 -3", "0 -90 0"]];
            break;
          case "#Bedroom1":
            mapper = [0,0,0,0,["1.4 -1 -4", "0 -10 0"],0,["4 0 0.5", "0 -110 0"],0,0,0,0];
            break;
          case "#Toilet1":
            mapper = [0,0,0,0,0,["2.5 0 -3", "0 0 0"],0,0,0,0,0];
            break;
          case "#Bedroom2":
            mapper = [0,0,0,0,["-3 -1 -5", "0 0 0"],0,0,0,["7 -1 1", "0 -90 0"],0,0];
            break;
          case "#Toilet2":
            //console.log("Checking why this position is weird");
            mapper = [0,0,0,0,0,0,0,["0.5 -1 5", "0 180 0"],0,0,0];
            break;
          case "#Bedroom3":
            mapper = [0,0,0,0,["3 -1 4", "0 210 0"],0,0,0,0,0,0];
            break;
          case "#CommonToilet":
            mapper = [0,0,0,0,["3 -0.5 0.5", "0 -90 0"],0,0,0,0,0,0];
            break;
          case "#ActivityArea":
            mapper = [0,["-0.5 -1.5 5","0 -300 0"], ["-3 -1 -0.5","0 82 0"],0,0,0,0,0,0,0,0];
            break;
          case "#Bathroom":
            mapper = [0,["-0.5 -1.5 5","0 -300 0"], ["-3 -1 -0.5","0 180 0"],0,0,0,0,0,0,0,0];
            break;
        }

        // console.log(mapper);
        // console.log(mapper.length);
        //Enable navlinks mapper, controls when navlinks are available.

        for (var i = navlinks.length - 1; i >= 0; i--) {
          var cngeometry = navlinks[i].childNodes[0].getAttribute("geometry");

          if (mapper[i] == 0) {
            cngeometry.height = 0;
            cngeometry.width = 0;
          } else {
            cngeometry.height = 1;
            cngeometry.width = 1;
          }

          navlinks[i].childNodes[0].setAttribute("geometry", cngeometry);
          if(typeof mapper[i] == "object" && typeof mapper[i][0] == "string" && typeof mapper[i][1] == "string") {
            navlinks[i].setAttribute("position", mapper[i][0]);
            navlinks[i].setAttribute("rotation", mapper[i][1]);
          }
        }
      }, data.dur);
    });
  },

  /**
   * Toggle a node's visibility
   */

  /**
   * Setup fade-in + fade-out.
   */
  setupFadeAnimation: function () {
    var data = this.data;
    var targetEl = this.data.target;

    // Only set up once.
    if (targetEl.dataset.setImageFadeSetup) { return; }
    targetEl.dataset.setImageFadeSetup = true;

    // Create animation.
    targetEl.setAttribute('animation__fade', {
      property: 'material.color',
      startEvents: 'set-image-fade',
      dir: 'alternate',
      dur: data.dur,
      from: '#FFF',
      to: '#000',
    });
  }
});

AFRAME.registerComponent('arrowbutton',{
  schema: {
    event: {type:'string'},
    navto: {type: 'string'},
  },  

  init: function () {
    var el = this.el;
    var data = this.data;
    el.addEventListener(data.event, function () {
      console.log("clicked on " + data.navto);
    });
  }
});
